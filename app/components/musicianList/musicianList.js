'use strict';

angular.module('rflApp.musicianList', [])
.directive('musicianList', [function() {
  return {
    restrict:  'E',
    scope: {
      'musicians': '='
    },
    templateUrl: 'components/musicianList/musicianList.html'
  }
}]);
