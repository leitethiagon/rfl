'use strict';

angular.module('rflApp.musicianDetails', [])
.directive('musicianDetails', [function() {
  return {
    restrict:  'E',
    scope: {
      musician: '='
    },
    templateUrl: 'components/musicianDetails/musicianDetails.html'
  }
}]);
