'use strict';

angular.module('rflApp.bandMembers', [])
.directive('bandMembers', function() {
  return {
    require: '^^musicianList',
    restrict: 'E',
    scope: {
      title: '@'
    },
    link: function(scope, element, attrs, musiciansCtrl) {
      musiciansCtrl.addPane(scope);
    }
  };
});
