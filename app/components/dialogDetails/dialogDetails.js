'use strict';

angular.module('rflApp.dialogDetails', [])
.directive('dialogDetails', [function() {
  return {
    restrict:  'E',
    scope: {
      'dialog': '='
    },
    controller: ['$scope', function DialogDetailsController($scope) {
      var options = $scope.dialog.options || [];

      $scope.respond = function(a) {
        angular.forEach(options, function(option) {
          option.selected = false;
        });
        option.selected = true;
      };

      this.getAnswer = function() {
        return options.filter(function(option) { return option.selected; });
      };
    }],
    templateUrl: 'components/dialogDetails/dialogDetails.html'
  }
}]);
