angular.module('rflApp.myBandDetails', [])
.directive('myBandDetails', [function() {
  return {
    restrict:  'E',
    scope: {
      band: '='
    },
    templateUrl: 'components/myBandDetails/myBandDetails.html'
  }
}]);
