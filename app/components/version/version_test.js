'use strict';

describe('rflApp.version module', function() {
  beforeEach(module('rflApp.version'));

  describe('version service', function() {
    it('should return current version', inject(function(version) {
      expect(version).toEqual('0.1');
    }));
  });
});
