'use strict';

angular.module('rflApp.version', [
  'rflApp.version.interpolate-filter',
  'rflApp.version.version-directive'
])

.value('version', '0.1');
