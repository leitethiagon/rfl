'use strict';

// Declare app level module which depends on views, and components
var rflApp = angular.module('rflApp', [
  'ngRoute',
  'rflApp.home',
  'rflApp.view2',
  'rflApp.myBand',
  'rflApp.version',
  'rflApp.musicianDetails',
  'rflApp.myBandDetails',
  'rflApp.dialogDetails',
  'rflApp.dialogChallenge',
  'rflApp.musicianList',
  'rflApp.createBand',
  'rflApp.challengeService'
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');

  //$routeProvider.otherwise({redirectTo: '/home'});
}]);
