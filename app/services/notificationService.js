angular.module('rflApp.userNotificationService', [])
.service('userNotificationService', [function() {
  return {
    notify: notify
  }

  function notify(message) {
    console.log(message);
  }
}]);
