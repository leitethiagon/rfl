angular.module('rflApp.bandService', [])
.value('bandData', {
  'name': 'MOSTO',
  'profile': 'Duis at lacinia elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque eu ultricies nisi. Nullam congue egestas bibendum. Etiam ultrices, massa et lobortis rhoncus, justo libero pulvinar orci, at laoreet leo nulla at nisi. Donec placerat sagittis auctor. Vivamus tempor efficitur erat.',
  'fans': 0,
  'cash': 0,
  'experience': 0,
  'gear': 0,
  'musicians': [
    {
      'name' : 'Jimmy Heindez',
      'profile' : 'Jimmy is a musician by heart and talented born. Doesnt know shit about theory, but when it starts playing it blows everyoness minds',
      'instrument' : 'Guitar',
      'experience' : 0, // (Todos - Gear, Temper)
      'performance': 0,
      'charisma' : 0,
      'technique' : 0,
      'criativity': 0,
      'attitude' : 0,
      'teamwork' : 0,
      'happiness': 0,
      'gear' : 0,
      'temper' : 0
    },
    {
      'name' : 'Matt Fieldman',
      'profile' : 'Also known as BASS DEMON',
      'instrument' : 'Bass',
      'experience' : 0, // (Todos - Gear, Temper)
      'performance': 0,
      'charisma' : 0,
      'technique' : 0,
      'criativity': 0,
      'attitude' : 0,
      'teamwork' : 0,
      'happiness': 0,
      'gear' : 0,
      'temper' : 0
    }
  ]
})
.service('bandService', ['bandData', function(bandData) {
  return {
    getBandDetails: function() {
      return bandData;
    }
  }
}]);
