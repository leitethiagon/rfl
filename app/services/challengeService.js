'use strict';

angular.module('rflApp.challengeService', [])
.value('challengesData', [
  {
    'id': 1,
    'title': 'Montando a banda',
    'text': 'os zamigo se reunem e monta uma banda',
    'options': [
      { 'id': 1, 'text': 'Entra na banda' },
      { 'id': 2, 'text': 'Recusa o convite' }
    ]
  },
  {
    'id': 2,
    'title': 'Mas e agora',
    'text': 'Ok, mas não sabemos tocar nada, o que você vai tocar?',
    'options': [
      { 'id': 1, 'text': 'Guitarra' },
      { 'id': 2, 'text': 'Bateria' }
    ]
  },
  {
    'id': 3,
    'title': 'A importunante groupie',
    'text': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et nibh ante. Maecenas rhoncus quam vitae urna dapibus aliquam. Pellentesque mi neque, scelerisque ut mi vel, mollis bibendum felis. Morbi interdum tristique augue, id consequat odio maximus a. In ac tristique dui, vel tempor eros. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed id sodales ligula. Nullam ex eros, dapibus non justo sed, scelerisque scelerisque ipsum. Donec non nisi vitae nibh auctor rhoncus. Proin vulputate nisl sem, ut bibendum odio tempus eget. Donec vitae velit dictum neque dapibus convallis. Cras ornare euismod eros, in finibus est venenatis id. Nunc nulla nisl, molestie quis pellentesque vel, luctus a velit. Fusce suscipit et odio ac condimentum. Pellentesque justo lectus, consectetur id pulvinar nec, dictum sit amet eros. Nulla convallis volutpat tellus nec elementum.',
    'options': [
      { 'text': 'Você disfarça e vira as costas' },
      { 'text': 'Você começa uma conversa' }
    ]
  }
])
.service('challengeService', ['challengesData', function (challengesData) {
  return {
    get: getNextChallenge,
    respond: respond
  };

  function getNextChallenge() {
    return challengesData.length && challengesData.length > 0 ? challengesData[challengesData.length - 1] : undefined;
  };

  function respond(answer) {
    challengesData.pop();
  };
}]);
