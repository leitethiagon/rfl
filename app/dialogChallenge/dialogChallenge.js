'use strict';

angular.module('rflApp.dialogChallenge', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/challenge', {
    templateUrl: 'dialogChallenge/dialogChallenge.html',
    controller: 'DialogChallengeCtrl'
  });
}])
.controller('DialogChallengeCtrl', ['$scope', 'challengeService', '$location', function($scope, challengeService, $location) {
  $scope.dialog = challengeService.get();

  $scope.respond = function(answer) {
    challengeService.respond(answer);

    $scope.dialog = challengeService.get();
  };

  this.getAnswer = function() {
    return $scope.dialog.options.filter(function(option) { return option.selected; });
  };
}]);
