'use strict';

angular.module('rflApp.home', ['ngRoute', 'rflApp.dialogFlow'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/home', {
    templateUrl: 'home/home.html',
    controller: 'homeCtrl'
  });
}])

.controller('homeCtrl', [function() {
  
}]);
