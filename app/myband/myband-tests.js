'use strict';

describe('Controller: MyBandCtrl', function () {

  // load the controller's module
  beforeEach(module('rflApp.myband'));

  var MybandCtrl, scope, $httpBackend;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope, _$httpBackend_) {
    $httpBackend = _$httpBackend_;
    $httpBackend.expectGET('/api/myband')
      .respond({
        'name': 'my band',
        'fans': 0,
        'cash': 0,
        'experience': 0,
        'gear': 0,
        'musicians': [
          {
            'name' : 'jimmy',
            'instrument' : 'guitar',
            'experience' : 0, // (Todos - Gear, Temper)
            'performance': 0,
            'charisma' : 0,
            'technique' : 0,
            'criativity': 0,
            'attitude' : 0,
            'teamwork' : 0,
            'happiness': 0
            'gear' : 0,
            'temper' : 0
          },
          {
            'name' : 'matty',
            'instrument' : 'bass',
            'experience' : 0, // (Todos - Gear, Temper)
            'performance': 0,
            'charisma' : 0,
            'technique' : 0,
            'criativity': 0,
            'attitude' : 0,
            'teamwork' : 0,
            'happiness': 0
            'gear' : 0,
            'temper' : 0
          }
        ]
      });

    scope = $rootScope.$new();
    MybandCtrl = $controller('MybandCtrl', {
      $scope: scope
    });
  }));

  it('should be able to display band information', function () {
    expect(scope.name).toBeDefined();
    expect(scope.fans).toBeDefined();
    expect(scope.cash).toBeDefined();
    expect(scope.experience).toBeDefined();
    expect(scope.gear).toBeDefined();
    expect(scope.musicians).toBeDefined();
    expect(scope.musicians.length).toBeDefined();
  });
});
