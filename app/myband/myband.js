'use strict';

angular.module('rflApp.myBand', ['ngRoute', 'rflApp.bandService'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/myband', {
    templateUrl: 'myband/myband.html',
    controller: 'MyBandCtrl',
    caseInsensitiveMatch: true
  });
}])
.controller('MyBandCtrl', ['$scope', 'bandService', function($scope, bandService) {
  $scope.band = bandService.getBandDetails();
}]);
