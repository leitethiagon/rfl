angular.module('rflApp.createBand', [])
.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/createband', {
    templateUrl: 'createband/createband.html',
    controller: 'CreateBandController',
    caseInsensitiveMatch: true
  });
}])
.controller('CreateBandController', ['$scope', function($scope) {
  $scope.bandMusicians = [];
  $scope.availableMusicians = [
    {
      'name' : 'Jimmy Heindez',
      'profile' : 'Jimmy is a musician by heart and talented born. Doesnt know shit about theory, but when it starts playing it blows everyoness minds',
      'instrument' : 'Guitar',
      'experience' : 0, // (Todos - Gear, Temper)
      'performance': 0,
      'charisma' : 0,
      'technique' : 0,
      'criativity': 0,
      'attitude' : 0,
      'teamwork' : 0,
      'happiness': 0,
      'gear' : 0,
      'temper' : 0
    },
    {
      'name' : 'Matt Fieldman',
      'profile' : 'Also known as BASS DEMON',
      'instrument' : 'Bass',
      'experience' : 0, // (Todos - Gear, Temper)
      'performance': 0,
      'charisma' : 0,
      'technique' : 0,
      'criativity': 0,
      'attitude' : 0,
      'teamwork' : 0,
      'happiness': 0,
      'gear' : 0,
      'temper' : 0
    }
  ];

  var bandMusicians = $scope.bandMusicians;

  $scope.bandMusicianSelected = function(musician) {
    var index = bandMusicians.indexOf(musician);
    bandMusicians.splice(index, 1);
  }

  $scope.availableMusicianSelected = function (musician) {
    var isSelectionValid = true;

    var index = bandMusicians.indexOf(musician);
    if(index !== -1)
      isSelectionValid = false;

    var musiciansSameInstrument = bandMusicians.filter(function(m) { return m.instrument == musician.instrument; });
    if(musician.instrument == 'Guitar' && musiciansSameInstrument.length >= 2)
        isSelectionValid = false;
    else if(musiciansSameInstrument.length >= 1)
      isSelectionValid = false;

    if(isSelectionValid)
      bandMusicians.push(musician);
  }
}])
.directive('createBand', [function() {
  return {
    restrict:  'E',
    scope: { },
    templateUrl: 'createBand/createBand.html'
  }
}]);
